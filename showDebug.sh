#!/bin/bash

# This script filters the debug.txt file to show only lines that do not contain the words "ConnectionSend" or "removed/deactivated"
# since they can be very noisy. If you need to filter out more words, you can add them to the grep pipe below.
grep -wv ConnectionSend debug.txt | grep -wv removed/deactivated
