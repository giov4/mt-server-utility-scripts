# This command shows deprecations warnings:
#
# 1. `grep -E 'deprecated|Separator' debug.txt`
#    Searches the file `debug.txt` for lines containing the words "deprecated" or "Separator" (case-insensitive)
#
# 2. `sed 's/WARNING\[/\n\nWARNING\[/'`
#    Adds two newlines before each occurrence of "WARNING["
#
# 3. `sed 's/Separator/\n\n-------------\n  Separator\n-------------\n\n/'`
#    Replaces each occurrence of "Separator" with a block of text surrounded by dashes
#
# 4. `stdbuf -o0`
#    Ensures that the output is not buffered, so that the output can be read by the next command.
#
# 5. `bash -c "..."`
#    Runs the entire command as a Bash script

bash -c "stdbuf -o0 grep -E 'deprecated|Separator' debug.txt | sed 's/WARNING\[/\n\nWARNING\[/' | sed 's/Separator/\n\n-------------\n  Separator\n-------------\n\n/'"
