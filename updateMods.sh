#!/bin/bash

# This script updates the git repositories of mods in the specified folder
# It takes a list of strings as input and searches for folders containing those strings in their names
# For each matching folder, it enters the folder, pulls the latest changes from the git repository, and then exits the folder

mods_folder="mods"

clear

echo
echo "==========================="
echo

# Get the strings to search for
read -p "Mods to update separated by space (e.g: brawl murder): " strings

# Convert the strings into an array
IFS=' ' read -r -a array <<< "$strings"

echo
echo "==========================="

# Save the current directory
current_dir=$(pwd)

# For each subfolder in the base folder
for dir in "$mods_folder"/*; do
  # Check if it's a folder
  if [ -d "$dir" ]; then
    # Get the base name of the folder
    folder_name=$(basename "$dir")
    # For each string in the array
    for string in "${array[@]}"; do
      # Check if the subfolder contains the string in its name (case-insensitive)
      if [[ "${folder_name,,}" == *"${string,,}"* ]]; then
        echo
        echo "===== UPDATING $folder_name"
        # Enter the subfolder
        pushd "$dir" > /dev/null
        # Execute git pull
        git pull
        echo
        # Return to the previous directory
        popd > /dev/null
        # Exit the inner loop
        break
      fi
    done
  fi
done

echo "==========================="

echo
