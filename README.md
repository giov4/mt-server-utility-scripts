# MT Server Utility Scripts

A collection of useful scripts.

If any of these scripts are not recognized by your OS, try running `dos2unix <script_name.sh>` on them.