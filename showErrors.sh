# This code is a Bash command that shows error logs:

# 1. `stdbuf -o0 grep -P 'ServerError|ModError|ERROR|Lua|stack traceback|Separator|\t' debug.txt`
#    This command searches for lines in the file 'debug.txt' that contain any of the following patterns:
#    - 'ServerError'
#    - 'ModError'
#    - 'ERROR'
#    - 'Lua'
#    - 'stack traceback'
#    - 'Separator'
#    - A tab character ('\t')
#    The `stdbuf -o0` part ensures that the output is not buffered, so that the output can be read by the next command.

# 2. `| stdbuf -o0 grep -wv 'WARNING'  | stdbuf -o0 grep -wv 'ACTION' | stdbuf -o0 grep -wv 'CurlFetch'`
#    This command filters out any lines that contain the words 'WARNING', 'ACTION' or 'CurlFetch' (with word boundaries).

# 3. `| sed 's/ServerError/\n\n\nServerError/'`
#    This command replaces any occurrences of 'ServerError' with three newlines followed by 'ServerError'.

# 4. `| sed 's/ModError/\n\n\nModError/'`
#    This command replaces any occurrences of 'ModError' with three newlines followed by 'ModError'.

# 5. `| sed 's/Separator/\n\n-------------\n  Separator\n-------------\n\n/'`
#    This command replaces any occurrences of 'Separator' with a formatted string that includes newlines and dashes.

bash -c "stdbuf -o0 grep -P 'ServerError|ModError|ERROR|Lua|stack traceback|Separator|\t' debug.txt | stdbuf -o0 grep -wv 'WARNING' | stdbuf -o0 grep -wv 'ACTION' | stdbuf -o0 grep -wv 'CurlFetch' | sed 's/Serv>